//
//  PathFinder.h
//  IsoGame
//
//  Created by Przemek on 27.05.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import "CGPointArray.h"
#import "Global.h"

@interface PathFinder : NSObject

extern Global global;

@property int iniX;
@property int iniY;
@property int finX;
@property int finY;

@property (strong, nonatomic) NSArray *level;
@property (strong, nonatomic) NSMutableDictionary *openList;
@property (strong, nonatomic) NSMutableDictionary *closedList;
@property (strong, nonatomic) CGPointArray *path;

- (instancetype)init:(int)xIni iniY:(int)yIni finX:(int)xFin finY:(int)yFin lvlData:(NSArray *)lvldata;
- (CGPointArray *)findPath;

@end


@interface PathNode : NSObject

@property int x;
@property int y;
@property int g;
@property int h;

@property (strong, nonatomic) PathNode *parentNode;

- (instancetype)init:(int)xPos posY:(int)yPos valG:(int)gVal valH:(int)hVal link:(PathNode *)link;

@end
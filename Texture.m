//
//  Texture.m
//  IsoGame
//
//  Created by Przemek on 26.05.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#import "Texture.h"

@implementation Texture

@end


@implementation TextureDroid

@synthesize textures2D = _textures2D;
@synthesize texturesIso = _texturesIso;

+ (instancetype)sharedInstance {
    static dispatch_once_t pred;
    static id sharedInstance = nil;
    
    dispatch_once(&pred, ^{
        sharedInstance = [[ self alloc ] init ];
    });
    
    return sharedInstance;
}

- (void)dealloc {
    abort();
}

- (instancetype)init {
    self = [ super init ];
    
    _texturesIso = @{
                     @(Idle) : @[
                             [ SKTexture textureWithImageNamed:[ NSString stringWithFormat:@"iso_3d_%@", textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(N) andAction:Idle ] ) ] ],
                             [ SKTexture textureWithImageNamed:[ NSString stringWithFormat:@"iso_3d_%@", textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(NE) andAction:Idle ] ) ] ],
                             [ SKTexture textureWithImageNamed:[ NSString stringWithFormat:@"iso_3d_%@", textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(E) andAction:Idle ] ) ] ],
                             [ SKTexture textureWithImageNamed:[ NSString stringWithFormat:@"iso_3d_%@", textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(SE) andAction:Idle ] ) ] ],
                             [ SKTexture textureWithImageNamed:[ NSString stringWithFormat:@"iso_3d_%@", textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(S) andAction:Idle ] ) ] ],
                             [ SKTexture textureWithImageNamed:[ NSString stringWithFormat:@"iso_3d_%@", textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(SW) andAction:Idle ] ) ] ],
                             [ SKTexture textureWithImageNamed:[ NSString stringWithFormat:@"iso_3d_%@", textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(W) andAction:Idle ] ) ] ],
                             [ SKTexture textureWithImageNamed:[ NSString stringWithFormat:@"iso_3d_%@", textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(NW) andAction:Idle ] ) ] ]
                             ],
                     
                     @(Move) : @[
                             [ SKTexture textureWithImageNamed:[ NSString stringWithFormat:@"iso_3d_%@", textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(N) andAction:Move ] ) ] ],
                             [ SKTexture textureWithImageNamed:[ NSString stringWithFormat:@"iso_3d_%@", textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(NE) andAction:Move ] ) ] ],
                             [ SKTexture textureWithImageNamed:[ NSString stringWithFormat:@"iso_3d_%@", textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(E) andAction:Move ] ) ] ],
                             [ SKTexture textureWithImageNamed:[ NSString stringWithFormat:@"iso_3d_%@", textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(SE) andAction:Move ] ) ] ],
                             [ SKTexture textureWithImageNamed:[ NSString stringWithFormat:@"iso_3d_%@", textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(S) andAction:Move ] ) ] ],
                             [ SKTexture textureWithImageNamed:[ NSString stringWithFormat:@"iso_3d_%@", textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(SW) andAction:Move ] ) ] ],
                             [ SKTexture textureWithImageNamed:[ NSString stringWithFormat:@"iso_3d_%@", textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(W) andAction:Move ] ) ] ],
                             [ SKTexture textureWithImageNamed:[ NSString stringWithFormat:@"iso_3d_%@", textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(NW) andAction:Move ] ) ] ]
                             ]
                     };
    
    _textures2D = @{
                    @(Idle) : @[
                            [ SKTexture textureWithImageNamed:textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(N) andAction:Idle ]) ],
                            [ SKTexture textureWithImageNamed:textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(NE) andAction:Idle ]) ],
                            [ SKTexture textureWithImageNamed:textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(E) andAction:Idle ]) ],
                            [ SKTexture textureWithImageNamed:textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(SE) andAction:Idle ]) ],
                            [ SKTexture textureWithImageNamed:textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(S) andAction:Idle ]) ],
                            [ SKTexture textureWithImageNamed:textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(SW) andAction:Idle ]) ],
                            [ SKTexture textureWithImageNamed:textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(W) andAction:Idle ]) ],
                            [ SKTexture textureWithImageNamed:textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(NW) andAction:Idle ]) ]
                            ],
                    
                    @(Move) : @[
                            [ SKTexture textureWithImageNamed:textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(N) andAction:Move ]) ],
                            [ SKTexture textureWithImageNamed:textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(NE) andAction:Move ]) ],
                            [ SKTexture textureWithImageNamed:textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(E) andAction:Move ]) ],
                            [ SKTexture textureWithImageNamed:textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(SE) andAction:Move ]) ],
                            [ SKTexture textureWithImageNamed:textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(S) andAction:Move ]) ],
                            [ SKTexture textureWithImageNamed:textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(SW) andAction:Move ]) ],
                            [ SKTexture textureWithImageNamed:textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(W) andAction:Move ]) ],
                            [ SKTexture textureWithImageNamed:textureImage([[ Tile alloc ] initWithType:@(Droid) direction:@(NW) andAction:Move ]) ]
                            ]
                    };
    
    
    
    return self;
}

@end


NSString *textureImage( Tile *tile ) {
    switch ( tile.type ) {
        case Droid:
            switch ( tile.action ) {
                case Idle:
                    switch ( tile.direction ) {
                        case N:
                            return @"droid_n";
                            break;
                            
                        case NE:
                            return @"droid_ne";
                            break;
                            
                        case E:
                            return @"droid_e";
                            break;
                            
                        case SE:
                            return @"droid_se";
                            break;
                            
                        case S:
                            return @"droid_s";
                            break;
                            
                        case SW:
                            return @"droid_sw";
                            break;
                            
                        case W:
                            return @"droid_w";
                            break;
                            
                        case NW:
                            return @"droid_nw";
                    }
                    
                case Move:
                    switch ( tile.direction ) {
                        case N:
                            return @"droid_n";
                            break;
                            
                        case NE:
                            return @"droid_ne";
                            break;
                            
                        case E:
                            return @"droid_e";
                            break;
                            
                        case SE:
                            return @"droid_se";
                            break;
                            
                        case S:
                            return @"droid_s";
                            break;
                            
                        case SW:
                            return @"droid_sw";
                            break;
                            
                        case W:
                            return @"droid_w";
                            break;
                            
                        case NW:
                            return @"droid_nw";
                    }
            }
            break;
            
        case Ground:
            return @"ground";
            break;
            
        case Wall:
            switch ( tile.direction ) {
                case N:
                    return @"wall_n";
                    break;
                    
                case NE:
                    return @"wall_ne";
                    break;
                    
                case E:
                    return @"wall_e";
                    break;
                    
                case SE:
                    return @"wall_se";
                    break;
                    
                case S:
                    return @"wall_s";
                    break;
                    
                case SW:
                    return @"wall_sw";
                    break;
                    
                case W:
                    return @"wall_w";
                    break;
                    
                case NW:
                    return @"wall_nw";
            }
    }
}

//
//  PathFinder.m
//  IsoGame
//
//  Created by Przemek on 27.05.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#import "PathFinder.h"
#import "Helpers.h"

@implementation PathFinder {
    int moveCostHorizontalOrVertical;
    int moveCostDiagonal;
}

- (instancetype)init:(int)xIni iniY:(int)yIni finX:(int)xFin finY:(int)yFin lvlData:(NSArray *)lvldata {
    self = [ super init ];
    
    moveCostHorizontalOrVertical = 10;
    moveCostDiagonal = 14;
    
    _iniX = xIni;
    _iniY = yIni;
    _finX = xFin;
    _finY = yFin;
    _level = lvldata;
    _openList = [ NSMutableDictionary new ];
    _closedList = [ NSMutableDictionary new ];
    _path = [ CGPointArray new ];
    
    //invert y coordinates - pre conversion (spriteKit inverted coordinate system). This PathFnding code ONLY works with positive (absolute) values
    
    _iniY = -_iniY;
    _finY = -_finY;
    
    PathNode *node = [ [ PathNode alloc ] init:_iniX posY:_iniY valG:0 valH:0 link:nil ];
    
    [ _openList setObject:node forKey:[NSString stringWithFormat:@"%d %d", _iniX, _iniY] ];
    
    return self;
}

- (CGPointArray *)findPath {
    
    [ self searchLevel ];
    
    //invert y cordinates - post conversion
    
    CGPointArray *pathWithYInversionRestored = [ CGPointArray new ];
    CGPointArray *reversed = [ CGPointArray new ];
    
    for ( int i = 0; i < self.path.count; i++ ) {
        [ pathWithYInversionRestored setPoint:mulCGPoints([pathWithYInversionRestored pointAtIndex:i], CGPointMake(1, -1)) atIndex:i ]; // Mapping
    
        for ( int r = self.path.count-1; r>=0; r-- ) {
            [ reversed setPoint:[ pathWithYInversionRestored pointAtIndex:i ] atIndex:r ];
        }     //Reversing
    }
    
    return reversed;
}

- (void)searchLevel {
    
    PathNode *curNode;
    PathNode *endNode;
    
    int lowF = 100000;
    BOOL finished = NO;
    
    for ( PathNode *obj in self.openList ) {
        CGFloat curF = obj.g + obj.h;
        
        if ( lowF > curF ) {
            lowF = curF;
            curNode = obj;
        }
    }
    
    if ( curNode == nil ) {
        return;
    } else {
        NSString *listKey = [ NSString stringWithFormat:@"%d %d", curNode.x, curNode.y ];
        
        [ self.openList setObject:nil forKey:listKey ];
        [ self.closedList setObject:curNode forKey:listKey ];
        
        if ( (curNode.x == self.finX ) && (curNode.y == self.finY) ) {
            endNode = curNode;
            finished = YES;
        }
        
        
        for ( int i = -1; i < 2; i++ ) {
            for ( int j = -1; j < 2; j++ ) {
                int col = curNode.x + i;
                int row = curNode.y + j;
                
                NSArray *level0 = [ NSArray new ];
                level0 = [ self.level objectAtIndex:0 ];
                
                if ( (col >= 0 && col < level0.count) && (row >= 0 && row < self.level.count) && (i != 0 || j != 0) ) {
                    listKey = [ NSString stringWithFormat:@"%d %d", col, row ];
                    
                    if ( ([[(NSArray *)[self.level objectAtIndex:row] objectAtIndex:col ] intValue ] == global.tilePath.traversable) && ([[self.closedList objectForKey:listKey ] isEqual:nil]) && ([[self.openList objectForKey:listKey] isEqual:nil]) ) {
                        
                        BOOL moveIsAllowed = YES;
                        
                        if ( (i != 0) && (j != 0) ) {
                            //is diagonal move
                            
                            if ( (i == -1) && (j == -1) ) {
                                if ( [[[self.level objectAtIndex:row] objectAtIndex:(col+1)] intValue ] != global.tilePath.traversable || [[[self.level objectAtIndex:(row+1)] objectAtIndex:col] intValue] != global.tilePath.traversable ) {
                                    moveIsAllowed = NO;
                                }
                            } else if ( (i == 1) && (j == -1) ) {
                                if ( [[[self.level objectAtIndex:row] objectAtIndex:(col-1)] intValue ] != global.tilePath.traversable || [[[self.level objectAtIndex:(row+1)] objectAtIndex:col] intValue] != global.tilePath.traversable ) {
                                    moveIsAllowed = NO;
                                }
                            } else if ( (i == -1) && (j == 1) ) {
                                if ( [[[self.level objectAtIndex:row] objectAtIndex:(col+1)] intValue ] != global.tilePath.traversable || [[[self.level objectAtIndex:(row-1)] objectAtIndex:col] intValue] != global.tilePath.traversable ) {
                                    moveIsAllowed = NO;
                                }
                            } else if ( (i == 1) && (j == 1) ) {
                                if ( [[[self.level objectAtIndex:row] objectAtIndex:(col-1)] intValue ] != global.tilePath.traversable || [[[self.level objectAtIndex:(row-1)] objectAtIndex:col] intValue] != global.tilePath.traversable ) {
                                    moveIsAllowed = NO;
                                }
                            }
                        }
                        
                        if ( moveIsAllowed ) {
                            int g;
                            
                            if ( (i != 0) && (j != 0) ) {
                                g = moveCostDiagonal;
                            } else {
                                g = moveCostHorizontalOrVertical;
                            }
                            
                            int h = [ self heuristicWithRow:row andCol:col ];
                            
                            [ self.openList setObject:[[ PathNode alloc ] init:col posY:row valG:g valH:h link:curNode ] forKey:listKey ];
                            
                        }
                    }
                }
            }
        }
        
        if ( finished == NO ) {
            [ self searchLevel ];
        } else {
            [ self retracePath:endNode ];
        }
    }
    
}



- (int)heuristicWithRow:(int)row andCol:(int) col {
    int xDistance = abs(col - self.finX);
    int yDistance = abs(row - self.finY);
    
    if ( xDistance > yDistance ) {
        return moveCostDiagonal*yDistance + moveCostHorizontalOrVertical*(xDistance-yDistance);
    } else {
        return moveCostDiagonal*xDistance + moveCostHorizontalOrVertical*(yDistance-xDistance);
    }
    
}

- (void)retracePath:(PathNode *)node {
    
    CGPoint step = CGPointMake(node.x, node.y);
    
    [ self.path addPoint:step ];
    
    if ( node.g > 0 ) {
        [ self retracePath:node.parentNode ];
    }
}

@end


@implementation PathNode

- (instancetype)init:(int)xPos posY:(int)yPos valG:(int)gVal valH:(int)hVal link:(PathNode *)link {
    self = [ super init ];
    
    _x = xPos;
    _y = yPos;
    _g = gVal;
    _h = hVal;
    
    if ( link !=nil ) {
        _parentNode = link;
    } else {
        _parentNode = nil;
    }
    
    return self;
}

@end



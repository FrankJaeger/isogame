//
//  Helpers.m
//  IsoGame
//
//  Created by Przemek on 25.05.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#import "Helpers.h"

@implementation Helpers

@end

CGPoint addCGPoints( CGPoint p1, CGPoint p2 ) {
    return CGPointMake( p1.x + p2.x, p1.y + p2.y );
}

CGPoint subCGPoints( CGPoint p1, CGPoint p2 ) {
    return CGPointMake( p1.x - p2.x, p1.y - p2.y );
}

CGPoint mulCGPoints( CGPoint p1, CGPoint p2 ) {
    return CGPointMake( p1.x * p2.x, p1.y * p2.y );
}

CGPoint divCGPoints( CGPoint p1, CGPoint p2 ) {
    return CGPointMake( p1.x / p2.x, p1.y / p2.y );
}


CGPoint point2DToPointTileIndex( CGPoint point, CGSize tileSize ) {
    CGPoint tp = divCGPoints(point, CGPointMake(tileSize.width, tileSize.height));
    
    tp.x = floor(tp.x);
    tp.y = floor(tp.y);
    
    return tp;
}

CGPoint pointTileIndexToPoint2D( CGPoint point, CGSize tileSize ) {
    
    return mulCGPoints(point, CGPointMake(tileSize.width, tileSize.height));
}
//
//  Global.h
//  IsoGame
//
//  Created by Przemek on 01.06.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#ifndef IsoGame_Global_h
#define IsoGame_Global_h

typedef struct Global Global;

struct Global {
    struct tilePath {
        int traversable;
        int nonTraversable;
    } tilePath;
};

#endif

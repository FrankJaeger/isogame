//
//  GameScene.h
//  IsoGame
//

//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "Global.h"

@interface GameScene : SKScene

@end

extern Global global;
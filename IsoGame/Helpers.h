//
//  Helpers.h
//  IsoGame
//
//  Created by Przemek on 25.05.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

@interface Helpers : NSObject

@end

CGPoint addCGPoints( CGPoint p1, CGPoint p2 );
CGPoint subCGPoints( CGPoint p1, CGPoint p2 );
CGPoint mulCGPoints( CGPoint p1, CGPoint p2 );
CGPoint divCGPoints( CGPoint p1, CGPoint p2 );

CGPoint point2DToPointTileIndex( CGPoint point, CGSize tileSize );
CGPoint pointTileIndexToPoint2D( CGPoint point, CGSize tileSize );

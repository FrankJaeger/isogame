//
//  CGPointArray.h
//  IsoGame
//
//  Created by Przemek on 31.05.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

@interface CGPointArray : NSObject {
    CGPoint *array;
}

@property int count;

- (void)addPoint:(CGPoint)point;
- (void)setPoint:(CGPoint)point atIndex:(int)index;
- (CGPoint)pointAtIndex:(int)index;

@end

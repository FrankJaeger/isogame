//
//  Tile.m
//  IsoGame
//
//  Created by Przemek on 24.05.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#import "Tile.h"

@implementation Tile

- (instancetype)initWithType:(NSNumber *)type {
    self = [ super init ];
    _type = [ type intValue ];
    
    return self;
}

- (instancetype)initWithType:(NSNumber *)type direction:(NSNumber *)direction {
    self       = [ super init ];
    _type      = [ type intValue ];
    _direction = [ direction intValue ];
    _action = Idle;
    
    return self;
}

- (instancetype)initWithType:(NSNumber *)type direction:(NSNumber *)direction andAction:(action)action{
    self = [ super init ];
    _type      = [ type intValue ];
    _direction = [ direction intValue ];
    _action    = action;
    
    return self;
}

- (NSString *)tileDescription {
    switch ( self.type ) {
        case Ground:
            return @"Ground";
            break;
            
        case Wall:
            return @"Wall";
            break;
            
        case Droid:
            return @"Droid";
            break;
    }
    
    return @"Wall";
}

- (NSString *)directionDescription {
    switch ( self.direction ) {
        case N:
            return @"North";
            break;
            
        case NE:
            return @"North East";
            break;
            
        case E:
            return @"East";
            break;
            
        case SE:
            return @"South East";
            break;
            
        case S:
            return @"South";
            break;
            
        case SW:
            return @"South West";
            break;
            
        case W:
            return @"West";
            break;
            
        case NW:
            return @"North West";
            break;
    }
    
    return @"East";
}

- (NSString *)actionDescription {
    switch ( self.action ) {
        case Idle:
            return @"Idle";
            break;
            
        case Move:
            return @"Move";
            break;
    }
    
    return @"Idle";
}

@end

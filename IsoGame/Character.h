//
//  Character.h
//  IsoGame
//
//  Created by Przemek on 26.05.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>
#import "Tile.h"

@protocol TileObject <NSObject>

@property tiletype tile;

@end

@interface Character : NSObject

@property direction facing;
@property action action;

@property (strong, nonatomic) SKSpriteNode *tileSprite2D;
@property (strong, nonatomic) SKSpriteNode *tileSpriteIso;

@end


@interface Android : Character <TileObject>

- (void)update;

@end
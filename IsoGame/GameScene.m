//
//  GameScene.m
//  IsoGame
//
//  Created by Przemek on 24.05.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#import "GameScene.h"
#import "Tile.h"
#import "Helpers.h"
#import "Texture.h"
#import "Character.h"
#import "PathFinder.h"


@implementation GameScene {
    SKSpriteNode *view2D;
    SKSpriteNode *viewIso;
    
    SKNode *layerIsoGround;
    SKNode *layerIsoObjects;
    SKNode *layer2DHighlight;
    
    NSArray *tiles;
    
    CGSize tileSize;
    
    Android *hero;
    
    int nthFrame;
    int nthFrameCount;
}

- (instancetype)initWithSize:(CGSize)size {
    view2D  = [ SKSpriteNode new ];
    viewIso = [ SKSpriteNode new ];
    layerIsoGround = [ SKNode new ];
    layerIsoObjects = [ SKNode new ];
    layer2DHighlight = [ SKNode new ];
    
    nthFrame = 6;
    nthFrameCount = 0;
    
     tiles = @[
             @[ @[@1, @7], @[@1, @0], @[@1, @0], @[@1, @0], @[@1, @0], @[@1, @0], @[@1, @1] ],
             @[ @[@1, @6], @[@0, @0], @[@0, @0], @[@0, @0], @[@0, @0],@[@0, @0],  @[@1, @2] ],
             @[ @[@1, @6], @[@0, @0], @[@2, @2], @[@0, @0], @[@0, @0],@[@0, @0],  @[@1, @2] ],
             @[ @[@1, @6], @[@0, @0], @[@0, @0], @[@1, @5], @[@1, @4],@[@1, @4],  @[@1, @3] ],
             @[ @[@1, @6], @[@0, @0], @[@0, @0], @[@0, @0], @[@0, @0],@[@0, @0],  @[@0, @0] ],
             @[ @[@1, @6], @[@0, @0], @[@0, @0], @[@0, @0], @[@0, @0],@[@0, @0],  @[@0, @0] ],
             @[ @[@1, @5], @[@1, @4], @[@1, @4], @[@1, @4], @[@1, @4],@[@1, @4],  @[@1, @3] ],
              ];
    
    tileSize.width  = 32;
    tileSize.height = 32;
    
    hero = [ Android new ];
    
    self = [ super initWithSize:size ];
    
    self.anchorPoint = CGPointMake(0.5, 0.5);
    
    return self;
}

- (void)didMoveToView:(SKView *)view {
    CGFloat deviceScale = self.size.width/667;
    
    view2D.position = CGPointMake(-self.size.width*0.45, self.size.height*0.17);
    view2D.xScale = deviceScale;
    view2D.yScale = deviceScale;
    
    [ self addChild:view2D ];
    
    layer2DHighlight.zPosition = 999;
    [ view2D addChild:layer2DHighlight ];
    
    viewIso.position = CGPointMake(self.size.width*0.12, self.size.height*0.12);
    viewIso.xScale = deviceScale;
    viewIso.yScale = deviceScale;
    
    [ viewIso addChild:layerIsoGround ];
    [ viewIso addChild:layerIsoObjects ];
    
    [ self addChild:viewIso ];
    
    [ self placeAllTiles2D ];
    [ self placeAllTilesIso ];
}

- (void)placeTile2D:(Tile *)tile withPosition:(CGPoint)point {
    SKSpriteNode *tileSprite = [ [ SKSpriteNode alloc ] initWithImageNamed:textureImage(tile) ];
    
    if ( tile.type == hero.tile ) {
        hero.tileSprite2D = tileSprite;
        hero.tileSprite2D.zPosition = 1;
    }
    
    tileSprite.position    = point;
    tileSprite.anchorPoint = CGPointMake(0, 0);
    
    [ view2D addChild:tileSprite ];
}

- (void)placeAllTiles2D {
    for ( int i = 0; i < tiles.count; i++ ) {
        NSArray *row = [ tiles objectAtIndex:i ];
        
        for ( int j = 0; j < row.count; j++ ) {
            Tile *tile = [ [ Tile alloc ] initWithType:[[ row objectAtIndex:j ] objectAtIndex:0]  direction:[[ row objectAtIndex:j ] objectAtIndex:1] ];
            
            CGPoint point = CGPointMake(j*tileSize.width, -(i*tileSize.height));
            
            if ( tile.type == Droid ) [ self placeTile2D:[[Tile alloc] initWithType:Ground direction:@(tile.direction) ] withPosition:point ];
            
            [ self placeTile2D:tile withPosition:point ];
        }
    }
}

- (CGPoint)point2DToIso:(CGPoint)point {
    CGPoint p = mulCGPoints( point, CGPointMake(1, -1) );
    
    p = CGPointMake( (p.x - p.y), ((p.x + p.y) / 2) );
    p = mulCGPoints( p, CGPointMake(1, -1) );
    
    return p;
}

- (CGPoint)pointIsoTo2D:(CGPoint)point {
    CGPoint p = mulCGPoints( point, CGPointMake(1, -1) );
    
    p = CGPointMake( ((2 * p.y + p.x) / 2), ((2 * p.y - p.x) / 2) );
    p = mulCGPoints( p, CGPointMake(1, -1) );
    
    return p;
}

- (void)placeTileIso:(Tile *)tile withPosition:(CGPoint)point {
    SKSpriteNode *tileSprite = [ [ SKSpriteNode alloc ] initWithImageNamed:[NSString stringWithFormat:@"iso_3d_%@", textureImage(tile)] ];
    
    if ( tile.type == hero.tile ) hero.tileSpriteIso = tileSprite;
    
    tileSprite.position    = point;
    tileSprite.anchorPoint = CGPointMake(0, 0);
    
    if ( tile.type == Ground ) {
        [ layerIsoGround addChild:tileSprite ];
    } else if ( tile.type == Wall || tile.type == Droid ) {
        [ layerIsoObjects addChild:tileSprite ];
    }
}

- (void)placeAllTilesIso {
    for ( int i = 0; i < tiles.count; i++ ) {
        NSArray *row = [ tiles objectAtIndex:i ];
        
        for ( int j = 0; j < row.count; j++ ) {
            Tile *tile = [ [ Tile alloc ] initWithType:[[ row objectAtIndex:j ] objectAtIndex:0] direction:[[ row objectAtIndex:j ] objectAtIndex:1] ];
            
            CGPoint point = [ self point2DToIso:( CGPointMake((j * tileSize.width), -(i * tileSize.height)) ) ];
            
            if ( tile.type == Droid ) [ self placeTileIso:[[Tile alloc] initWithType:Ground direction:@(tile.direction) ] withPosition:point ];
            
            [ self placeTileIso:tile withPosition:point ];
        }
    }
}


- (direction)degreesToDirection:(CGFloat)degrees {
    if ( degrees < 0 ) {
        degrees += 360;
    }
    
    CGFloat directionRange = 45.0;
    
    degrees += directionRange/2;
    
    int direction = (int)floor(degrees/directionRange);
    
    if ( direction == 8 ) {
        direction = 0;
    }
    
    return direction;
}

- (void)sortDepth {
    NSArray *childrenSortedForDepth = [ layerIsoObjects.children sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        CGPoint p0 = [ self pointIsoTo2D:[(SKSpriteNode *)obj1 position] ];
        CGPoint p1 = [ self pointIsoTo2D:[(SKSpriteNode *)obj2 position] ];
        
        if ( (p0.x+(-p0.y)) > (p1.x+(-p1.y)) ) {
            return true;
        } else {
            return false;
        }
    } ];
    
    
    for ( int i = 0; i < childrenSortedForDepth.count; i++ ) {
        SKNode *node = [ childrenSortedForDepth objectAtIndex:i ];
        
        node.zPosition = (CGFloat)i;
    }
    
}

- (NSArray *)traversableTiles {
    NSMutableArray *tTiles = [ NSMutableArray new ];
    
    for ( NSArray *row in tiles ) {
        NSMutableArray *tt = [ NSMutableArray new ];
        
        for ( NSArray *col in row ) {
            [ tt addObject:^{
                if ( [[col objectAtIndex: 0] intValue] == 1 ) {
                    return @(global.tilePath.nonTraversable);
                } else {
                    return @(global.tilePath.traversable);
                }
            }];
        }
        
        [ tTiles addObject:tt ];
    }
    
    return tTiles;
}

- (CGPointArray *)findPathFrom:(CGPoint)from To:(CGPoint)to {
    
    NSArray *traversable = [ NSArray new ];
    traversable = [ self traversableTiles ];
    
    if ( ((int)to.x > 0 ) && ((int)to.x < traversable.count) && ((int)-to.y > 0) && ((int)-to.y < traversable.count) ) {
        if ( [[[traversable objectAtIndex:(int)-to.y] objectAtIndex:(int)to.x] intValue] == global.tilePath.traversable ) {
            PathFinder *pathFinder = [[ PathFinder alloc ] init:(int)from.x iniY:(int)from.y finX:(int)to.x finY:(int)to.y lvlData:traversable ];
            
            CGPointArray *myPath = [ pathFinder findPath ];
            
            return myPath;
        } else {
            return nil;
        }
    } else {
        return nil;
    }
}

- (void)highlightPath2D:(CGPointArray *)path {
    [ layer2DHighlight removeAllChildren ];
    
    for ( int i = 0; i < path.count; i++ ) {
        SKSpriteNode *highlightTile = [ SKSpriteNode spriteNodeWithImageNamed:textureImage([[Tile alloc] initWithType:@(Ground) direction:@(N) andAction:Idle]) ];
        
        highlightTile.position = pointTileIndexToPoint2D([ path pointAtIndex:i ], tileSize);
        highlightTile.anchorPoint = CGPointMake(0, 0);
        
        highlightTile.color = [ SKColor colorWithRed:1 green:0 blue:0 alpha:0.25+((CGFloat)(i/path.count*0.25))];
        highlightTile.colorBlendFactor = 1;
        
        [ layer2DHighlight addChild:highlightTile ];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = touches.anyObject;
    CGPoint touchLocation = [ touch locationInNode:viewIso ];
    
    CGPoint touchPos2D = [ self pointIsoTo2D:touchLocation ];
    
    touchPos2D = addCGPoints(touchPos2D, CGPointMake(tileSize.width/2, -(tileSize.height/2)) );
    
    CGPointArray *pth = [ self findPathFrom:point2DToPointTileIndex(hero.tileSprite2D.position, tileSize) To:point2DToPointTileIndex(touchPos2D, tileSize) ];
    
    if ( pth != nil ) {
        
        CGPoint newHeroPos2D;
        CGPoint prevHeroPos2D = hero.tileSprite2D.position;
        NSMutableArray *actions = [ NSMutableArray new ];
        
        for ( int i = 1; i < pth.count; i++ ) {
            newHeroPos2D = pointTileIndexToPoint2D([pth pointAtIndex:i], tileSize);
            
            CGFloat deltaY = newHeroPos2D.y - prevHeroPos2D.y;
            CGFloat deltaX = newHeroPos2D.x - prevHeroPos2D.x;
            
            CGFloat degrees = atan2(deltaX, deltaY) * (180 / M_PI);
            
            [actions addObject:[ SKAction runBlock:^{
                hero.facing = [self degreesToDirection:degrees];
                [hero update];
            }] ];
            
            CGFloat velocity = tileSize.width*2;
            NSTimeInterval time = 0;
            
            if ( i == 1 ) {
                time = sqrt( pow(newHeroPos2D.x-hero.tileSprite2D.position.x, 2) + pow(newHeroPos2D.y-hero.tileSprite2D.position.y, 2) ) / velocity;
            } else {
                CGFloat baseDuration = tileSize.width/velocity;
                CGFloat multiplier = 1;
                
                direction direction = [self degreesToDirection:degrees];
                
                if ( direction == NE || direction == NW || direction == SW || direction == SE ) {
                    multiplier = 1.4;
                }
                
                time = multiplier*baseDuration;
            }
        
            [ actions addObject:[SKAction moveTo:newHeroPos2D duration:time] ];
            
            prevHeroPos2D = newHeroPos2D;
        }
        
        [hero.tileSprite2D removeAllActions];
        [hero.tileSprite2D runAction:[SKAction sequence:actions]];
        
        [ self highlightPath2D:pth ];
    }
}

- (void)update:(NSTimeInterval)currentTime {
    hero.tileSpriteIso.position = [ self point2DToIso:hero.tileSprite2D.position ];
    
    nthFrameCount++;
    
    if ( nthFrameCount == nthFrame ) {
        nthFrameCount = 0;
        [ self updateOnNthFrame ];
    }
}

- (void)updateOnNthFrame {
    [ self sortDepth ];
}

@end

//
//  Character.m
//  IsoGame
//
//  Created by Przemek on 26.05.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#import "Character.h"
#import "Texture.h"

@implementation Character

- (instancetype)init {
    self = [ super init ];
    
    _facing = E;
    _action = Idle;
    
    return self;
}

@end


@implementation Android

@synthesize tile = _tile;

- (instancetype)init {
    self = [ super init ];
    
    _tile = Droid;
    
    return self;
}

- (void)update {
    if ( self.tileSpriteIso != nil ) {
        self.tileSpriteIso.texture = [[[ TextureDroid sharedInstance ].texturesIso objectForKey:@(self.action)] objectAtIndex:self.facing ];
    }
    
    if ( self.tileSprite2D != nil ) {
        self.tileSprite2D.texture = [[[ TextureDroid sharedInstance ].textures2D objectForKey:@(self.action)] objectAtIndex:self.facing ];
    }
}

@end
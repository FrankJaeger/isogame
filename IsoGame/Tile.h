//
//  Tile.h
//  IsoGame
//
//  Created by Przemek on 24.05.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    Ground,
    Wall,
    Droid
} tiletype;

typedef enum {
    N,
    NE,
    E,
    SE,
    S,
    SW,
    W,
    NW
} direction;

typedef enum {
    Idle,
    Move
} action;

@interface Tile : NSObject


@property tiletype type;
@property direction direction;
@property action action;

- (instancetype)initWithType:(NSNumber *)type;
- (instancetype)initWithType:(NSNumber *)type direction:(NSNumber *)direction;
- (instancetype)initWithType:(NSNumber *)type direction:(NSNumber *)direction andAction:(action)action;

@end

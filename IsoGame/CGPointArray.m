//
//  CGPointArray.m
//  IsoGame
//
//  Created by Przemek on 31.05.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#import "CGPointArray.h"

@implementation CGPointArray

- (instancetype)init {
    self = [ super init ];
    
    _count = 0;
    
    array = calloc( _count, sizeof(*array) );
    
    return self;
}

- (void)dealloc {
    free( array );
}

- (void)addPoint:(CGPoint)point {
    self.count++;
    
    array = realloc( array, self.count*sizeof(*array) );
    
    array[self.count-1] = point;
}

- (CGPoint)pointAtIndex:(int)index {
    return array[index];
}

- (void)setPoint:(CGPoint)point atIndex:(int)index {
    if ( self.count < index+1 ) {
        self.count = index+1;
        array = realloc( array, self.count*sizeof(*array) );
    }
    
    array[index] = point;
}

@end

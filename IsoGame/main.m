//
//  main.m
//  IsoGame
//
//  Created by Przemek on 24.05.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Global.h"

Global global;

int main(int argc, char * argv[]) {
    
    global.tilePath.traversable = 0;
    global.tilePath.nonTraversable = 1;
    
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

//
//  Texture.h
//  IsoGame
//
//  Created by Przemek on 26.05.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Tile.h"
#import <SpriteKit/SpriteKit.h>

@protocol TextureObject <NSObject>

+(instancetype)sharedInstance;

@property (strong, nonatomic) NSDictionary *texturesIso;
@property (strong, nonatomic) NSDictionary *textures2D;

@end

@interface TextureDroid : NSObject <TextureObject>

@end

@interface Texture : NSObject

@end

NSString *textureImage( Tile *tile );